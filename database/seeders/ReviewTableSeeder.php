<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ReviewTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory('App\Models\Review', 150)->create();
        $reviews = \App\Library\Data\FetchJsonFile::open('reviews.json');

        foreach ($reviews as $review)
        {
            $product = \App\Models\Product::inRandomOrder()->first();
            $user = \App\Models\Auth\User::inRandomOrder()->first();
            \App\Models\Review::create([
               'product_id' => $product->id,
               'user_id' => $user->id,
               'stars' => $review['stars'],
               'review' => $review['review']
            ]);
        }
    }
}
