<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::defaultView('vendor.pagination.bootstrap-4');

        Paginator::defaultSimpleView('vendor.pagination.simple-bootstrap-4');

        if ($this->app->environment('local', 'testing')) {

        }
        if($this->app->isLocal())
        {
            $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);
        }

	    view()->composer(['*'], function ($view) {


	        $appCurrency = getCurrency(config('app.currency'));

	        // if (Schema::hasTable('locales')) {
	        //     $locales = Locale::pluck('short_name as locale')->toArray();
	        // }

	        $view->with(compact('appCurrency'));

	    });

    }
}
