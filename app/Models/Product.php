<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Carbon\Carbon;
use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    //use Searchable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id', 'tax_id', 'image_id', 'title', 'price', 'description', 'weight'
    ];

    /**
     * returns the image path
     *
     * @return string
     */
    public function path()
    {
        return (isset($this->image)) ? $this->image->path : 'empty';
    }

    /**
     * returns the thumbnail path
     *
     * @return string
     */
    public function thumbnail()
    {
        return (isset($this->image)) ? $this->image->thumbnail : 'empty';
    }

    /**
     * gets the price value and formats it with two digits
     *
     * @param $value
     * @return string
     */
    public function getPriceAttribute($value)
    {
        return number_format($value, 2, '.', ' ');
    }

    /**
     * price value is always set using money format 2 digets
     *
     * @param $value
     */
    public function setPriceAttribute($value)
    {

        $this->attributes['price'] = number_format($value, 2, '.', ' ');
    }

    /**
     * is the product on sale
     *
     * @return bool
     */
    public function hasSale()
    {
        $current = $this->sales
            ->where('start', '<=', Carbon::now()->format('Y-m-d'))
            ->where('finish', '>=', Carbon::now()->format('Y-m-d'))
            ->count();
        return ($current >= 1) ? true : false;
    }

    /**
     * returns the sale price
     *
     * @return string
     */
    public function salePrice()
    {
        $discount = 1 - $this->sales()->current()->first()->discount;

        return number_format($this->price * $discount, 2);
    }

    /**
     * a product belongs to a category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    /**
     * a product has many reviews
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reviews()
    {
        return $this->hasMany('App\Models\Review');
    }

    /**
     * a product has a single image
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function image()
    {
        return $this->belongsTo('App\Models\Image');
    }

    /**
     * a product has a tax
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tax()
    {
        return $this->belongsTo('App\Models\Tax');
    }

    /**
     * a product can have many sales
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sales()
    {
        return $this->hasMany('App\Models\Sale');
    }

    /**
     * performs a search if algolia is not used
     * comment out if algolia is used
     *
     * @param $query
     * @return mixed
     */
    public function search($query)
    {
        return $this->where('title', 'like', "%$query%");
    }
}
