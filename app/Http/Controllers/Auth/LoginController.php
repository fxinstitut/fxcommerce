<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }



    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255',
            'password' => 'required|min:4'
            // 'g-recaptcha-response' => (config('access.captcha.registration') ? ['required',new CaptchaRule] : ''),
        // ],[
        //     'g-recaptcha-response.required' => __('validation.attributes.frontend.captcha'),
        // ]
        ]);

        if($validator->passes()){

            $credentials = $request->only($this->username(), 'password');
            $authSuccess = Auth::attempt($credentials, $request->has('remember'));

            if($authSuccess) {
                $request->session()->regenerate();
                $redirect = 'back';
                // if(Auth::user()->isAdmin()){
                //     $redirect = 'dashboard';
                // }else{

                // }
                return response(['success' => true,'redirect' => $redirect], Response::HTTP_OK);

            }else{
                return
                    response([
                        'success' => false,
                        'message' => 'Login failed. Account not found'
                    ], Response::HTTP_FORBIDDEN);
            }

        }


        return response(['success'=>false,'errors' => $validator->errors()]);

    }
}
