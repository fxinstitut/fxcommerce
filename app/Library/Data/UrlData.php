<?php
/*
|--------------------------------------------------------------------------
| UrlData.php
|--------------------------------------------------------------------------
| Created by Shawn Legge
| This class is responsible for returning a list of common urls that will be
| used by vue
*/

namespace App\Library\Data;


use App\Http\Controllers\Auth\RegisterValidationController;



class UrlData
{
    public static function get()
    {
        return [
            'state_url' => route('user.states'),
            'address_url' => route('address.index'),
            'order_url' => route('order.add.api'),
            'billing_url' => route('order.billing.post'),
            'users_url' => route('users.details'),
            'user_order_url' => str_replace('/:id', '', route('user.account.order', [':id'])),
            'search_url' => str_replace('/:search', '', route('search.products.search', [':search'])),

            'show_product_url' => str_replace('/:product', '', route('product.show', [':product'])),

            'shopping_cart' => route('shopping.cart'),
            'shopping_cart_add' => route('shopping.cart.add'),
            'shopping_cart_delete' => route('shopping.cart.destroy'),
            'shopping_cart_update' => route('shopping.cart.update'),
            'category_url' => str_replace('/:category', '', route('search.product.category', [':category'])),
            'user_email_url' => action([RegisterValidationController::class, 'email']),

            // 'user_username_url' => action([RegisterValidationController::class, 'username'])
        ];
    }
}

