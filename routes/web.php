<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CartController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\User\OrderController;
use App\Http\Controllers\Admin\SalesController;
use App\Http\Controllers\Admin\TaxesController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\User\SearchController;
use App\Http\Controllers\Admin\OrdersController;
use App\Http\Controllers\Admin\StatesController;
use App\Http\Controllers\User\API\UserController;
use App\Http\Controllers\User\ProductsController;
use App\Http\Controllers\User\API\StateController;
use App\Http\Controllers\Admin\AdminHomeController;
use App\Http\Controllers\Admin\CategoriesController;
use App\Http\Controllers\User\API\AddressController;
use App\Http\Controllers\User\API\BillingController;
use App\Http\Controllers\User\UserAccountController;
use App\Http\Controllers\Admin\API\TaxesAPIController;
use App\Http\Controllers\Admin\API\OrdersAPIController;
use App\Http\Controllers\Admin\API\StatesAPIController;
use App\Http\Controllers\Admin\API\ProductsAPIController;
use App\Http\Controllers\User\API\ShoppingCartController;
use App\Http\Controllers\Admin\API\CategoriesAPIController;
use App\Http\Controllers\Auth\RegisterValidationController;
use App\Http\Controllers\User\API\OrderController as ApiOrderController;
use App\Http\Controllers\User\API\SearchController as ApiSearchController;
use App\Http\Controllers\Admin\ProductsController as AdminProductsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Auth routes
Route::get('logout', [LoginController::class, 'logout'])->name('user.logout');
Auth::routes();

//Register User
Route::post('user-email-taken', [RegisterValidationController::class, 'email'])->name('register.user.email');
Route::post('user-username-taken', [RegisterValidationController::class, 'username'])->name('register.user.username');
Route::get('get-states', [StateController::class, 'index'])->name('user.states');

// Home Routes
Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('about', [HomeController::class, 'about'])->name('about');
Route::get('contact', [HomeController::class, 'contact'])->name('contact');
Route::get('view-product/{title}', [ProductsController::class, 'show'])->name('product.show');

// Search
Route::get('product/{category}', [SearchController::class, 'category'])->name('search.product.category');
Route::post('products', [SearchController::class, 'scout'])->name('search.products');
Route::get('products/{search}', [SearchController::class, 'show'])->name('search.products.search');
Route::post('products/api', [ApiSearchController::class, 'store'])->name('search.products.api');

// Shopping Cart Routes
Route::post('shopping-cart/add', [CartController::class, 'addToCart'])->name('shopping.cart.add');
Route::post('shopping-cart/remove', [CartController::class, 'remove'])->name('shopping.cart.destroy');
Route::post('shopping-cart/update', [CartController::class, 'update'])->name('shopping.cart.update');
Route::get('shopping-cart/get', [CartController::class, 'index'])->name('shopping.cart');

//User Account
Route::get('user/user-account', [UserAccountController::class, 'index'])->name('user.account');
Route::get('user/user-account/order/{id}', [UserAccountController::class, 'show'])->name('user.account.order');
Route::get('user/users-details', [UserController::class, 'index'])->name('users.details');

//orders Page
Route::get('/order/{stage}', [OrderController::class, 'index'])->name('order.index');
Route::post('/order', [ApiOrderController::class, 'store'])->name('order.add.api');
Route::patch('/order/{order}', [ApiOrderController::class, 'update'])->name('order.update.api');
Route::post('/order/billing-form', [BillingController::class, 'store'])->name('order.billing.post');
Route::resource('user/address', AddressController::class);

//orders API
Route::get('/order/invoice/{order}', [ApiOrderController::class, 'show'])->name('order.invoice.api');



Route::group(['middleware' => 'auth'], function () {
    Route::post('cart/checkout', [CartController::class, 'checkout'])->name('cart.checkout');
    Route::post('cart/add', [CartController::class, 'addToCart'])->name('cart.addToCart');
    Route::get('cart', [CartController::class, 'index'])->name('cart.index');
    Route::get('cart/clear', [CartController::class, 'clear'])->name('cart.clear');
    Route::get('cart/remove', [CartController::class, 'remove'])->name('cart.remove');
    Route::post('cart/apply-coupon',[CartController::class, 'applyCoupon'])->name('cart.applyCoupon');
    Route::post('cart/remove-coupon',[CartController::class, 'removeCoupon'])->name('cart.removeCoupon');
    Route::post('cart/stripe-payment', [CartController::class, 'stripePayment'])->name('cart.stripe.payment');
    Route::post('cart/paypal-payment', [CartController::class, 'paypalPayment'])->name('cart.paypal.payment');
    Route::get('cart/paypal-payment/status', [CartController::class, 'getPaymentStatus'])->name('cart.paypal.status');

    Route::get('status', function () {
        return view('frontend.cart.status');
    })->name('status');
    Route::post('cart/offline-payment', [CartController::class, 'offlinePayment'])->name('cart.offline.payment');
    Route::post('cart/getnow',[CartController::class, 'getNow'])->name('cart.getnow');
});

if(config('show_offers') == 1){
    Route::get('offers',[CartController::class, 'getOffers'])->name('frontend.offers');
}



/*
|--------------------------------------------------------------------------
| Admin Section
|--------------------------------------------------------------------------
| The following routes are from the admin section of Lara-Commerce.  In order
| to access these pages the user must have admin privileges.
*/
Route::group(['as' => 'admin.'], function(){
    Route::get('admin', [AdminHomeController::class, 'index'])->name('user.account');;

    //Products
    Route::resource('admin/products', AdminProductsController::class);
    Route::post('admin/products/image/{product}', [ProductsAPIController::class, 'image'])->name('products.image.api');
    Route::get('admin/products-api/', [ProductsAPIController::class, 'index'])->name('products.api');


    // Sales
    Route::resource('admin/products/sales', SalesController::class);

    // Users
    Route::get('admin/users', [UsersController::class, 'index'])->name('users');;
    Route::get('admin/users/addresses/{user}', [UsersController::class, 'addresses'])->name('user.address');
    Route::get('admin/users/orders/{user}', [UsersController::class, 'orders'])->name('user.orders');

    // Orders
    Route::get('admin/orders', [OrdersController::class, 'index'])->name('orders');
    Route::get('admin/orders/{id}', [OrdersController::class, 'show'])->name('order');
    Route::get('admin/orders/view/{order}', [OrdersAPIController::class, 'show'])->name('order.show.api');
    Route::post('admin/orders/view/{order}', [OrdersAPIController::class, 'store'])->name('order.add.api');

    // Categories
    Route::get('admin/categories', [CategoriesController::class, 'index'])->name('categories');
    Route::resource('admin/api/categories', CategoriesAPIController::class);

    //Taxes
    Route::get('admin/taxes', [TaxesController::class, 'index'])->name('taxes');
    Route::get('admin/api/taxes/all', [TaxesAPIController::class, 'all'])->name('taxes.all.api');
    Route::resource('admin/api/taxes', TaxesAPIController::class);

    //States
    Route::get('admin/states', [StatesController::class, 'index'])->name('states');
    Route::resource('admin/api/states', StatesAPIController::class, ['only' => [
        'index', 'store', 'update', 'destroy'
    ]]);
});
Route::get('/test', function(){
    dd(\Illuminate\Support\Facades\Schema::hasTable('roles'));

    return view('test');
});


Route::post('/test', function(\Illuminate\Http\Request $request){


})->middleware('ajax.auth');;

// Route::prefix('auth')->group( function (){
//     Route::get('login', 'AppController@login');
//     Route::get('register', 'AppController@register');
//     Route::get('logout', 'AppController@logout');
// });

// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
