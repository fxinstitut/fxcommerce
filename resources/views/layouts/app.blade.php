<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="An e-commerce application">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'FXcommerce') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}">
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
            'stripeKey' => config('services.stripe.key'),
            'urls'      => \App\Library\Data\UrlData::get()
        ]); ?>
    </script>
</head>
<body>
        <div id="app">

            <main-page inline-template >
                <div>
                    <success-message v-show="showComponents.showMessage" :message="message">
                    </success-message>

                    <error-message
                    v-show="showComponents.showError"
                    :error-message="errorMessage"
                    >
                    </error-message>
                    <search-products-screen v-if="showComponents.search" :token="'{{ csrf_token() }}'">
                    </search-products-screen>

                    <main-shopping-cart inline-template :show="showComponents">
                        <div>
                            @include('_includes._ecommerce._navbar')
                            <div class="container">
                                <div v-if="show.fullScreen">
                                    @include('_includes._ecommerce._messages')
                                    @yield('content')
                                </div>
                                <div class="row" v-else>
                                    <div class="col-md-2 position-absolute" style="left:100px;">
                                        @include('_includes._ecommerce._sidebar')
                                    </div>
                                    <div class="col-md-8 my-0 mx-auto">
                                        @include('_includes._ecommerce._messages')
                                        @yield('content')
                                    </div>
                                </div>
                            </div><!-- /.container -->
                        </div>
                    </main-shopping-cart>
                </div>
            </main-page>
        </div>
        @include('_includes._ecommerce._footer')
        @yield('scripts')
    <script src="{{ mix('js/app.js') }}" defer></script>
</body>
</html>
