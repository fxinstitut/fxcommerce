<div class="row carousel-holder">
    <div class="col-md-12">
        <div id="ecommerce-carousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#ecommerce-carousel" data-slide-to="0" class="active"></li>
                <li data-target="#ecommerce-carousel" data-slide-to="1"></li>
                <li data-target="#ecommerce-carousel" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="slide-image" src="{{ asset('images/headerimagescale.jpg') }}" alt="Image header of sale">
                </div>
                <div class="carousel-item">
                    <img class="slide-image"  src="{{ asset('images/headerimagescale2.jpg') }}" alt="Image header of sale two">
                </div>
            </div><!-- /.carousel-indicators -->
            <a class="carousel-control-prev" href="#ecommerce-carousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#ecommerce-carousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div><!-- /.carousel-example-generic -->
    </div><!-- /.col -->
</div><!-- /.row -->
