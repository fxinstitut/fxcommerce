/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.Event = new Vue();

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import Vue from 'vue';
import {
    ValidationProvider,
    ValidationObserver,
    extend,
    setInteractionMode
} from 'vee-validate';

import {
    required,
    email,
    alpha_spaces,
    min,
    confirmed,
    regex,
    between
} from "vee-validate/dist/rules";

setInteractionMode('lazy');

// Override the default message.
extend('required', {
    ...required,
    message: 'Veuillez remplir ce champ'
});
// Override the default message.
extend('email', {
    ...email,
    message: `L'email n'est pas valide`,
});
// Override the default message.
extend('alpha_spaces', {
    ...alpha_spaces,
    message: `Ce champ n'est pas valide`
});
// Override the default message.
extend('min', {
    ...min,
    message: "Le {_field_} n'est pas valide",
});

extend('confirmed', {
    ...confirmed,
    message: "La confirmation du {_field_} ne correspond pas"
});
extend('regex', regex);

extend('between', between);

extend("unique-email", {
    validate: (value) => {
        return axios
            .post(window.Laravel.urls.user_email_url, {
                email: value,
            })
            .then(function (response) {

                return {
                    valid: response.data["email"]
                };
            })
            .catch(function (error) {
                console.log(error);
            });
    },
    message: "Cette adresse email est déjà enregistrée.",
});
window._ = require('lodash');

Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);


/**
 * Add vue components to the project.
 */
Vue.component('main-page', require('./components/main-page.vue').default);
Vue.component('main-shopping-cart', require('./components/main-shopping-cart.vue').default)


//Auth Pages
Vue.component('registration-form', require('./components/auth/registration-form.vue').default);
Vue.component('login-form', require('./components/auth/login-form.vue').default);

//Shopping Cart Pages
Vue.component('add-cart-icon', require('./components/shopping-cart/add-cart-icon.vue').default);
Vue.component('remove-item-icon', require('./components/shopping-cart/remove-item-icon.vue').default);


//Functionality
Vue.component('confirm-modal', require('./components/functionality/modal/confirm-modal.vue').default);
Vue.component('form-modal', require('./components/functionality/modal/form-modal.vue').default);
Vue.component('message-modal', require('./components/functionality/modal/message-modal.vue').default);
Vue.component('review-stars', require('./components/functionality/review-stars.vue').default);
Vue.component('select-cart-quantity', require('./components/functionality/select-cart-quantity.vue').default)
Vue.component('view-message', require('./components/functionality/view-message.vue').default);
Vue.component('step-progress-bar', require('./components/order/step-progress-bar.vue').default);
Vue.component('full-screen', require('./components/functionality/screen/full-screen.vue').default);

// Animation
Vue.component('fade-out-animation', require('./components/functionality/animations/fade-out-animation.vue').default);
Vue.component('slide-out-animation', require('./components/functionality/animations/slide-in-animation.vue').default);

// Messages
Vue.component('success-message', require('./components/functionality/messages/success-message.vue').default);
Vue.component('error-message', require('./components/functionality/messages/error-message.vue').default);


//Navbar
Vue.component('navbar-cart', require('./components/navbar/navbar-cart.vue').default);
Vue.component('search-products', require('./components/navbar/search-products.vue').default);
Vue.component('search-products-screen', require('./components/navbar/search-products-screen.vue').default);
Vue.component('categories-navbar', require('./components/navbar/categories-navbar.vue').default);


//User Account
Vue.component('user-account', require('./components/account/user-account.vue').default);
Vue.component('user-account-header', require('./components/account/user-account-header.vue').default);
Vue.component('user-account-address', require('./components/account/user-account-address.vue').default);
Vue.component('user-account-order', require('./components/account/user-account-order.vue').default);

//orders Page
Vue.component('order-total-details', require('./components/order/shared/order-total-details.vue').default);
Vue.component('order-page', require('./components/order/order-page.vue').default);
Vue.component('order-details', require('./components/order/order-details.vue').default);
Vue.component('order-information', require('./components/order/confirm-cart/order-information.vue').default);
Vue.component('user-address', require('./components/shared-information/address/user-address.vue').default);
Vue.component('select-user-address', require('./components/order/address/select-user-address.vue').default);
Vue.component('user-address-form', require('./components/shared-information/address/user-address-form.vue').default);
Vue.component('order-payment', require('./components/order/payment/order-payment.vue').default);
Vue.component('billing-form', require('./components/order/payment/billing-form.vue').default);
Vue.component('add-new-address', require('./components/shared-information/address/add-new-address.vue').default);
Vue.component('user-order', require('./components/order/invoice/user-order.vue').default);
Vue.component('user-order-item', require('./components/order/confirm-cart/user-order-item.vue').default);
Vue.component('user-order-header', require('./components/order/shared/user-order-header.vue').default);

// Shared Information
Vue.component('shipping-address', require('./components/shared-information/address/shipping-address.vue').default);
Vue.component('user-details', require('./components/shared-information/user-details.vue').default);
Vue.component('address-box', require('./components/account/address-box.vue').default);
Vue.component('edit-address', require('./components/shared-information/address/edit-address.vue').default);
Vue.component('delete-address', require('./components/shared-information/address/delete-address.vue').default);
Vue.component('order-list-item', require('./components/shared-information/order/order-list-item.vue').default);

const app = new Vue({

    el: '#app'
});
