const mix = require('laravel-mix');
const webpack = require('webpack');

mix.webpackConfig({
    plugins: [
        // Fix for moment.js failed to find local error
        new webpack.ContextReplacementPlugin(/\.\/locale$/, 'empty-module', false, /js$/)
    ]
});

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js').vue()
    .sass('resources/sass/app.scss', 'public/css')
    .version()
    .sourceMaps()
    .copy('node_modules/font-awesome/fonts', 'public/fonts')
    .copy('node_modules/font-awesome/css/font-awesome.css', 'public/css/font-awesome.css')
    .copy('node_modules/bootstrap-sass/assets/fonts/bootstrap', 'public/fonts/bootstrap');



mix.js('resources/js/admin.js', 'public/js').vue()
    .sass('resources/sass/admin.scss', 'public/css');



if (mix.inProduction() || process.env.npm_lifecycle_event !== 'hot') {
    mix.version();
}
